const DSNV = "DSNV";
var listNV = [];

function luuLocalStorage() {
  let jsonDsnv = JSON.stringify(listNV);
  localStorage.setItem(DSNV, jsonDsnv);
}

var dataJson = localStorage.getItem(DSNV);

if (dataJson !== null) {
  let nvArr = JSON.parse(dataJson);

  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    var nv = new dataNV(
      item.tknv,
      item.name,
      item.email,
      item.password,
      item.date,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );
    listNV.push(nv);
  }
  renderlistNV(listNV);
}

function addNV() {
  var nv = layDuLieuTuForm();

  var isValid = true;
  // validate tài khoản
  isValid =
    kiemTraRong(nv.tknv, "tbTKNV", "Tài khoản không được để rỗng") &&
    kiemTra6KySo(nv.tknv, "tbTKNV", "Tối đa 6 ký Số");

  // validate họ và tên
  isValid =
    kiemTraRong(nv.name, "tbTen", "Tên không được để trống") &&
    kiemTraChu(nv.name, "tbTen", "Tên NV phải là chữ");

  // validate kiem tra email
  isValid =
    kiemTraRong(nv.email, "tbEmail", "email không được để rỗng") &&
    kiemTraEmail(nv.email, "tbEmail");

  // validate kiểm tra mất khẩu
  isValid =
    kiemTraRong(nv.password, "tbMatKhau", "mật khẩu không được để rỗng") &&
    kiemTraMatKhau(
      nv.password,
      "tbMatKhau",
      "mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)"
    );
  // validate kiểm tra ngày

  if (isValid) {
    listNV.push(nv);
    luuLocalStorage();
    renderlistNV(listNV);
  }
  resetForm();
}

function deleteNV(id) {
  // console.log("id: ", id);
  var viTri = timViTri(id, listNV);
  if (viTri !== -1) {
    listNV.splice(viTri, 1);
  }

  luuLocalStorage();
  renderlistNV(listNV);
}

function editNV(id) {
  var dom = document.getElementById("btnThem");
  dom.click();
  var viTri = timViTri(id, listNV);
  if (viTri == -1) return;

  var data = listNV[viTri];
  showThongTinLenForm(data);
}

function updateNV() {
  var data = layDuLieuTuForm();
  console.log("data: ", data);
  var viTri = timViTri(data.tknv, listNV);
  if (viTri == -1) return;

  listNV[viTri] = data;
  renderlistNV(listNV);
  luuLocalStorage;
}

function remove() {
  let remove = document.querySelectorAll(".sp-thongbao");
  remove.forEach((item) => {
    item.style.display = "none";
  });
}
