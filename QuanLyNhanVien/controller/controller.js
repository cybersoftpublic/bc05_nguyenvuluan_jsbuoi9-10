function layDuLieuTuForm() {
  var tknv = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var date = document.getElementById("datepicker").value;
  var luongCB = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;

  var data = new dataNV(
    tknv,
    name,
    email,
    password,
    date,
    luongCB,
    chucVu,
    gioLam
  );
  return data;
}

function renderlistNV(nvArr) {
  var contentHTML = "";
  for (var index = 0; index < nvArr.length; index++) {
    var currentNV = nvArr[index];

    var contentTr = `
    <tr>
    <td>${currentNV.tknv}</td>
    <td>${currentNV.name}</td>
    <td>${currentNV.email}</td>
    <td>${currentNV.date}</td>
    <td>${currentNV.chucVu}</td>
    <td>${currentNV.tongLuong()}</td>
    <td>${currentNV.xepLoai()}</td>
    <td><button onclick="deleteNV('${
      currentNV.tknv
    }')" class="btn btn-danger"> Xóa </button>

    <button onclick="editNV('${
      currentNV.tknv
    }')" class="btn btn-success"> Edit </button>
    </td>
   </tr>
   
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timViTri(id, xoaNV) {
  for (var index = 0; index < xoaNV.length; index++) {
    var item = xoaNV[index];

    if (item.tknv == id) {
      return index;
    }
  }
  return -1;
}

function showThongTinLenForm(edit) {
  document.getElementById("tknv").value = edit.tknv;
  document.getElementById("name").value = edit.name;
  document.getElementById("email").value = edit.email;
  document.getElementById("password").value = edit.password;
  document.getElementById("datepicker").value = edit.date;
  document.getElementById("luongCB").value = edit.luongCB;
  document.getElementById("chucvu").value = edit.chucVu;
  document.getElementById("gioLam").value = edit.gioLam;
}

function resetForm() {
  document.getElementById("table_list").reset();
}

function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
  document.getElementById(idErr).style.display = "block";
}
