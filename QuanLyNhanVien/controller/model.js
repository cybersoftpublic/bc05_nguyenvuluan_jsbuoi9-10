function dataNV(tknv, name, email, password, date, luongCB, chucVu, gioLam) {
  this.tknv = tknv;
  this.name = name;
  this.email = email;
  this.password = password;
  this.date = date;
  this.luongCB = luongCB;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    var tongLuong = 0;
    if (this.chucVu == "Sếp") {
      tongLuong = this.luongCB * 3;
    } else if (this.chucVu == "Trưởng phòng") {
      tongLuong = this.luongCB * 2;
    } else {
      tongLuong = this.luongCB;
    }
    return tongLuong;
  };
  this.xepLoai = function () {
    var xepLoai = "";
    if (this.gioLam >= 192) {
      xepLoai = "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      xepLoai = "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      xepLoai = "Nhân viên khá";
    } else {
      xepLoai = "Nhân viên trung bình";
    }
    return xepLoai;
  };
}
