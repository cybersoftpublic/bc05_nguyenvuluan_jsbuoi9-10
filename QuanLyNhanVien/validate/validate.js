function kiemTraTrung(id, listNV) {
  let index = timViTri(id, listNV);
  if (index !== -1) {
    showMessageErr("tbTKNV", "Tài khoản đã tồn tại");
    return false;
  } else {
    showMessageErr("tbTKNV", "");
    return true;
  }
}

function kiemTraRong(userInput, idErr, message) {
  if (userInput.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}

function kiemTraSo(value, idErr, message) {
  var reg = /^\d+$/;
  let isNumber = reg.test(value);
  if (isNumber) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
function kiemTraChu(userName, idErr, message) {
  var reg = /^[a-zA-Z]*$/;
  let isKySo = reg.test(userName);
  if (isKySo) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraEmail(value, idErr) {
  var reg =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  let isEmail = reg.test(value);
  if (isEmail) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, "Email không đúng quy định");
    return false;
  }
}

function kiemTra6KySo(value, idErr, message) {
  var reg = /^[a-zA-Z0-9]*$/;
  let isKySo = reg.test(value);
  if (isKySo && value.length <= 6) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}

function kiemTraMatKhau(value, idErr, message) {
  const reg =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/;
  let pass = reg.test(value);
  if (pass) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, message);
    return false;
  }
}
